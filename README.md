# Traffooze Machine Learning

### BigQuery Tables Schema 

This folder contains the JSON schema that we used to create our BigQuery Tables, which stores our traffic data 

### Lambda Traffic Flow Data Collection

This file contains the code that automatically extracts, pre-process the traffic flow data and load it into the BigQuery Data Warehouse

### Traffic Images for Traffic Count Data Collection

This code collects traffic images from data.gov.sg API and extracts the vehicle count using object detection model, so that it can be used for machine learning

### Integrate Weather Attributes to Traffic Count

This code integrates historical weather data to the traffic count data that we have collected

### Traffic Flow Predictions Generation

This code generates Traffic Flow predictions and appends it to a dedicated BigQuery table

### Traffic Count Predictions Generation

This code generates Traffic Count predictions and appends it to a dedicated BigQuery table

### Traffic Flow Predictions Generate and Update

This code generates Traffic Flow predictions data and merges it with the existing data, if it corresponds to an existing data, then we will update the data, if it is a new data, then it will be inserted

### Traffic Count Predictions Generate and Update

This code generates Traffic Count predictions data and merges it with the existing data, if it corresponds to an existing data, then we will update the data, if it is a new data, then it will be inserted