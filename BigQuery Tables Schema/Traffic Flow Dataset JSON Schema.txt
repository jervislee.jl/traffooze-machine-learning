[
  {
    "name": "road_id",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "timestamp",
    "type": "DATETIME",
    "mode": "NULLABLE"
  },
  {
    "name": "speed",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "speedUncapped",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "freeFlow",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "jamFactor",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "temperature",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "humidity",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "pressure",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "visibility",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "wind_speed",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "wind_degree",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "wind_gust",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "clouds",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "rain_3h",
    "type": "FLOAT",
    "mode": "NULLABLE"
  }
]
