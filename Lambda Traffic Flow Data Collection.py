import os
import json
import pandas as pd
import requests
from geopy.distance import geodesic
import pymongo
from datetime import datetime
from google.cloud import bigquery

def lambda_handler(event, context):

    event_time_str = event.get('time', None)
    if event_time_str:
        
        event_time = pd.to_datetime(event_time_str)
        
        asia_singapore_tz = 'Asia/Singapore'

        # Convert the event time from UTC to 'Asia/Singapore'
        event_time = event_time.tz_convert(asia_singapore_tz)
        
        event_time = event_time.strftime('%Y-%m-%d %H:%M')  # Convert to 'YYYY-MM-DD HH:MM' format
    else:
        event_time = datetime.utcnow()

    mongo_uri = os.environ.get('MONGO_URI')
    here_apikey = os.environ.get('HERE_APIKEY')
    openweathermap_apikey = os.environ.get('OPENWEATHERMAP_APIKEY')

    response = requests.get(f"https://data.traffic.hereapi.com/v7/flow?in=circle:1.279683,103.809628;r=20830&locationReferencing=shape&apiKey={here_apikey}")
    data = response.json()["results"]
    raw_df = pd.DataFrame(data)
    
    raw_df = pd.concat([raw_df['location'].apply(pd.Series), raw_df['currentFlow'].apply(pd.Series)], axis=1)
    
    raw_df['coord_list'] = raw_df.apply(format_shape_as_list, axis=1)

    coord_combined = raw_df.groupby('description')['coord_list'].apply(lambda x: [shape for row in x for shape in row]).reset_index()

    raw_df = pd.merge(coord_combined, raw_df.drop('coord_list', axis=1), on='description')

    raw_df['shape'] = raw_df.apply(format_shape, axis=1)
    
    merged_df = raw_df.groupby('description')['shape'].apply(list).reset_index()
    
    merged_df['shape'] = merged_df['shape'].apply(lambda x: [{f'shape{i+1}': shape} for i, shape in enumerate(x)])
    
    df_merged = pd.merge(merged_df, raw_df.drop('shape', axis=1), on='description')
    
    columns_to_drop = ['subSegments', 'junctionTraversability', 'jamTendency', 'confidence']
    
    columns_to_drop = [col for col in columns_to_drop if col in df_merged.columns]
    
    df_merged = df_merged.drop(columns_to_drop, axis=1)
    
    open_df = df_merged.loc[df_merged['traversability'] == "open"].copy()
    
    open_df = open_df.drop("traversability", axis=1)
    
    open_df.dropna(inplace=True)
    
    duplicates = open_df.duplicated(subset='description', keep=False)
    duplicate_df = open_df[duplicates]
    
    unique_df = open_df[~open_df.duplicated(subset='description', keep=False)]
    
    road_aggregated_data = {}

    for _, row in duplicate_df.iterrows():
        road_name = row['description']
        shape = row['shape']
        coord_list = row['coord_list']
        length = row['length']
        speed = row['speed']
        speed_uncapped = row['speedUncapped']
        free_flow = row['freeFlow']
        jam_factor = row['jamFactor']
        
        
        if road_name in road_aggregated_data:
            (
                cumulative_length,
                cumulative_weighted_speed,
                cumulative_weighted_speed_uncapped,
                cumulative_weighted_free_flow,
                cumulative_weighted_jam_factor
            ) = road_aggregated_data[road_name][2:7]
            
            cumulative_length += length
            cumulative_weighted_speed += length * speed
            cumulative_weighted_speed_uncapped += length * speed_uncapped
            cumulative_weighted_free_flow += length * free_flow
            cumulative_weighted_jam_factor += length * jam_factor
            
            road_aggregated_data[road_name] = (
                shape,
                coord_list,
                cumulative_length,
                cumulative_weighted_speed,
                cumulative_weighted_speed_uncapped,
                cumulative_weighted_free_flow,
                cumulative_weighted_jam_factor
            )
        else:
            road_aggregated_data[road_name] = (
                shape,
                coord_list,
                length,
                length * speed,
                length * speed_uncapped,
                length * free_flow,
                length * jam_factor
            )

    road_average_speeds = {}

    for road_name, (
        shape,
        coord_list,
        cumulative_length,
        cumulative_weighted_speed,
        cumulative_weighted_speed_uncapped,
        cumulative_weighted_free_flow,
        cumulative_weighted_jam_factor
    ) in road_aggregated_data.items():
        average_speed = cumulative_weighted_speed / cumulative_length
        average_speed_uncapped = cumulative_weighted_speed_uncapped / cumulative_length
        average_free_flow = cumulative_weighted_free_flow / cumulative_length
        average_weighted_jam_factor = cumulative_weighted_jam_factor / cumulative_length
        
        road_average_speeds[road_name] = (
            shape,
            coord_list,
            cumulative_length,
            average_speed,
            average_speed_uncapped,
            average_free_flow,
            average_weighted_jam_factor
        )
    
    result_df = pd.DataFrame(list(road_average_speeds.values()), columns=['shape','coord_list','length', 'speed', 'speedUncapped', 'freeFlow', 'jamFactor'])
    result_df.insert(0, 'description', list(road_average_speeds.keys()))
    
    clean_df = pd.concat([unique_df, result_df])
    
    sorted_df = clean_df.sort_values("description")
    
    
    sorted_df['start_lat'] = sorted_df['shape'].apply(lambda x: x[0]['shape1'][0]['lat'])
    sorted_df['start_lng'] = sorted_df['shape'].apply(lambda x: x[0]['shape1'][0]['lng'])
    
    weather_response = requests.get(f"https://api.openweathermap.org/data/2.5/group?id=1880216,1880242,1880252,1880251,1880294,1880574,1880701,1880755,1882118,1882316,1882558,1882707,1882749,1882753&units=metric&appid={openweathermap_apikey}")
    
    weather_json = weather_response.json()
    
    weather_list = weather_json['list']
    
    for index, row in sorted_df.iterrows():
        road_coord = (row['start_lat'], row['start_lng'])
        
        closest_location = min(weather_list, key=lambda loc: calculate_distance(road_coord, (loc['coord']['lat'], loc['coord']['lon'])))
        
        sorted_df.loc[index, 'temperature'] = closest_location.get('main',{}).get('temp', 0)
        sorted_df.loc[index, 'humidity'] = closest_location.get('main',{}).get('humidity', 0)
        sorted_df.loc[index, 'pressure'] = closest_location.get('main',{}).get('pressure', 0)
        sorted_df.loc[index, 'visibility'] = closest_location.get('visibility', 0)
        sorted_df.loc[index, 'wind_speed'] = closest_location.get('wind',{}).get('speed', 0)
        sorted_df.loc[index, 'wind_degree'] = closest_location.get('wind',{}).get('deg', 0)
        sorted_df.loc[index, 'wind_gust'] = closest_location.get('wind',{}).get('gust', 0)
        sorted_df.loc[index, 'clouds'] = closest_location.get('clouds', {}).get('all', 0)
        sorted_df.loc[index, 'rain_3h'] = closest_location.get('rain',{}).get('3h', 0)
        
    sorted_df = sorted_df.sort_values("description")
    
    sorted_df['length'] = pd.to_numeric(sorted_df['length'], errors='coerce')
    sorted_df = sorted_df.dropna(subset=['length'])
    
    sorted_df['timestamp'] = event_time
    
    mongo_url = mongo_uri
    client = pymongo.MongoClient(mongo_url)
    db = client['TraffoozeDBS']
    collection = db['roads_metadata']
    
    try:
        # Fetch all existing descriptions from the MongoDB collection
        existing_descriptions = set()
        for doc in collection.find({}, {"description": 1}):
            existing_descriptions.add(doc["description"])
        
        max_road_id_document = collection.find_one(sort=[("road_id", pymongo.DESCENDING)], projection={"road_id": 1})

        if max_road_id_document:
            max_road_id = max_road_id_document["road_id"]
        else:
            max_road_id = 0
        
        # Prepare data for bulk insert
        bulk_data = []
        for index, row in sorted_df.iterrows():
            description = row['description']

            if description not in existing_descriptions:
                road_id = max_road_id + 1
                max_road_id += 1
                road_length = row['length']
                road_shape = row['shape']
                coord_list = row['coord_list']
                start_lat = row['start_lat']
                start_lng = row['start_lng']

                road_metadata = {
                    "road_id": road_id,
                    "description": description,
                    "length": road_length,
                    "shape": road_shape,
                    "coord_list": coord_list,
                    "start_lat": start_lat,
                    "start_lng": start_lng
                    
                }
                bulk_data.append(road_metadata)

        # Perform bulk insert if there is data to insert
        if bulk_data:
            collection.insert_many(bulk_data)

    except pymongo.errors.PyMongoError as e:
        # Handle MongoDB errors, if any
        return e
    finally:
        # Close the MongoDB connection after processing
        client.close()
    
    mongo_url = mongo_uri
    client = pymongo.MongoClient(mongo_url)
    db = client['TraffoozeDBS']
    collection = db['roads_metadata']
    
    road_mapping = {}
    for doc in collection.find({}, {"description": 1, "road_id": 1}):
        road_mapping[doc["description"]] = doc["road_id"]
    
    sorted_df['road_id'] = sorted_df['description'].map(road_mapping)
    
    client.close()
    
    # Drop the "description," "length,", "shape", "start_lat", and "start_lng" columns
    sorted_df = sorted_df.drop(['description', 'length', 'shape', 'start_lat', 'start_lng'], axis=1)
    
    sorted_df["timestamp"] = pd.to_datetime(sorted_df["timestamp"]).dt.strftime("%Y-%m-%d %H:%M:00")

    data_to_insert = sorted_df.to_dict(orient='records')
    
    service_account_info = {
    "type": os.environ.get("type"),
    "project_id": os.environ.get("project_id"),
    "private_key_id": os.environ.get("private_key_id"),
    "private_key": os.environ.get("private_key"),
    "client_email": os.environ.get("client_email"),
    "client_id": os.environ.get("client_id"),
    "auth_uri": os.environ.get("auth_uri"),
    "token_uri": os.environ.get("token_uri"),
    "auth_provider_x509_cert_url": os.environ.get("auth_provider_x509_cert_url"),
    "client_x509_cert_url": os.environ.get("client_x509_cert_url"),
    "universe_domain": os.environ.get("universe_domain")
    }
    
    client = bigquery.Client.from_service_account_info(service_account_info)

    dataset_id = 'traffooze'
    table_id = 'traffic_flow_data'
    
    table_ref = client.dataset(dataset_id).table(table_id)
    
    errors = client.insert_rows_json(table_ref, data_to_insert)
    
    if not errors:
        print('Data successfully inserted into BigQuery.')
    else:
        print('Encountered errors while inserting data into BigQuery:', errors)
    
    # Return the list of dictionaries as the response
    return {
        'statusCode': 200
        #'body': json.dumps(result_data)
    }
    
    
def format_shape(row):
    shape_dict = row['shape']
    links = shape_dict['links']
    coordinates = []
    for link in links:
        points = link['points']
        link_coordinates = [{'lat': point['lat'], 'lng': point['lng']} for point in points]
        coordinates.extend(link_coordinates)
    return coordinates
    
def format_shape_as_list(row):
    shape_dict = row['shape']
    links = shape_dict['links']
    coordinates = []
    for link in links:
        points = link['points']
        link_coordinates = [(point['lat'], point['lng']) for point in points]
        coordinates.extend(link_coordinates)
    return coordinates

def calculate_distance(coord1, coord2):
    return geodesic(coord1, coord2).meters
